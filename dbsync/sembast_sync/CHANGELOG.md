## 0.0.1-dev.4

- Added support for batch processing.

## 0.0.1-dev.3

- Fixed bug with missing await for clearAll.

## 0.0.1-dev.2

- Fixed bug with missing await for local changes deletion.
- Updated dependency

## 0.0.1-dev.1

- Initial version.
