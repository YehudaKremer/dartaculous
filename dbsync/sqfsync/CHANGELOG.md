## 0.0.1-dev.3

- Added support for batch processing.

## 0.0.1-dev.2

- Updated dependency.

## 0.0.1-dev.1

- Initial version.
