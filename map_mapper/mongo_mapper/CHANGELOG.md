## 0.2.0-dev.3

 - Update a dependency to the latest release.

## 0.2.0-dev.2

 - **FIX**: Changed repository URL.

## 0.2.0-dev.1

 - **FEAT**: removed dependecy from squarealfa_common_types.

## 0.2.0-dev.0

> Note: This release has breaking changes.

 - **BREAKING** **FEAT**: Huge upgrade to proto_generator.

## 0.1.0

 - Initial version.
